# VectorDrawable2Svg
This script converts your VectorDrawable to a Svg

## Usage
Drop one or more android vector drawable onto this script to convert them to svg format.

## Improvements
This script supports only a litte amount of attributes.

Suggestions (or merge request) for improvement are welcome

## Related question in stackoverflow.com
https://stackoverflow.com/questions/44948396/convert-vectordrawable-to-svg
